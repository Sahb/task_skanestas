## Task
The task is to find lag dependance on given futures historical data. These are, namely, 
10 YEAR TREASURY NOTE FUTURES, E-MINI S&P 500 FUTURES, FTSE CHINA A50 INDEX FUTURES.  


## 1. 
As a very first step we apply `pivot_table` to provided dataset. This restructures initial frame into form:
| ts | 10_year_treasury_note | e-mini_s&p_500  | ftse_china_a50_index |
|-----|---|---|---|
| 2020-01-01 20:01:00 | 128.296 | 3239.75 | 14497.5 |
| 2020-01-01 20:02:00 | 128.296	| 3239.50 | 14512.5 |
| 2020-01-01 20:03:00 | 128.296	| 3239.75 | 14515.0 |

One might see that time series start at different timestamps. Thus, starting and endings are truncated in a way that dataset starts and ends with numerical (not NA) values. The result is serialized as `df_pivot.pck`.


## 2. Short answer
refer to notebook `short_answer.ipynb`.
Here straightforward method for finding lag-relatinships between two time-series is provided. After a research on that topic, I chose two articles "Detection and clustering of lead-lag networks for multivariate time series with an application to financial markets" (provided in repo as article1) and "Lead–lag detection and network clustering for multivariate time series with an application to the US equity market" (article2) of the same authors. Article1 defines cross-correlation function (ccf) as
$$
ccf^{ij}(l) = corr(\{Y^i_{t-l}\}, \{Y^j_t\}),
$$
where *corr* means one functional type among (1) Pearson linear correlation, (2) Kendall rank correlation, (3) Distance correlation, (4) Mutual information based on discretised time series values [refer to supplementry notes A.1 in article 1]. I used Pearson linear correlation.
Function `cross_correlation` calculates correlation between `data1`, `data2`. This function is applied to different time-series pieces. By time-series I mean log returns which are stored in `df_pivot_log_pct`. As a check for the setup I chose duration of 30 minutes (have experimented with 20, 30, 40, 60 min). The `idx_start` and `idx_end` are used for data1 slicing.The second series is shifted for `lag` minutes. The overall results are stored in `cc_matr` dataframe. Then the overall ccf's which are > 0.3 are plotted.  

If possible, I would like to discuss this picture.
![corr](corr_ts.png "corr_ts")
This is a heatmap of log-return correlations which absolute values are > 0.3 for **10_year_treasury_note** and **e-mini_s&p_500**.


## 3. Another answer. 
Another possibility to calculate correlation is provided in article3 (formula 2) which states:
![lead_lag](lead_lag.png "lead_lag")
This approach is used in `another_answer.ipynb`.
After we define lag-effect according to that formula, article discusses toy example of 10 assets and 10 days, plots probability distribution of links. I could have repeated Fig.3 from that article. There is still a bug on getting random graph but the picture is more or less representative. This is done in 
`toy_example_10days.ipynb`. The article3 is interesting in sense that it proposes strategies on lead-lag relationship. It's possible to discuss it further and possibly worth to continue work on implementing strategies from there.


## Conclusion.
The lead-lag relationship is calculated by means of two options which are taken from two different articles (`article1`, `article3`). Both approaches I have used can be dicussed and updated. I have tried to follow `article3` further. Due to lack of experience and time I couldn't take further steps as of turning the lead-lag relationship into a strategy but it would be fun to get familiar how its works.
