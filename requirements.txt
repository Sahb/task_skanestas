dcor==0.6
matplotlib==3.7.2
networkx==3.1
numpy==1.23.5
p_tqdm==1.4.0
pandas==2.0.3
pickle5==0.0.12
scikit_learn==1.3.0
scipy==1.11.3
seaborn==0.13.0
statsmodels==0.14.0
tqdm==4.66.1
